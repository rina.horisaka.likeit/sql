﻿use practice1;

select
t1.category_name,
sum(t.item_price) as total_price
from
item t
left outer join
item_category t1
on
t.category_id = t1.category_id
group by
t.category_id
order by
total_price desc;
